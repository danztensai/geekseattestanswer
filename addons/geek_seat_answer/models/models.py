from odoo import fields, models, api

class SaleOrderCustomization(models.Model):
    _inherit = 'sale.order'
    # New custom field to store special instruction
    special_instruction = fields.Text(string="Special Instruction", compute="_compute_special_instruction", store=True)
    
    # Add a new state named 'approved'
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('pending', 'Pending'),  # New state
        ('sale', 'Sales Order'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False, index=True, tracking=3,
        default='draft', required=True)
    
    
    def action_set_pending(self):
        self.write({'state': 'pending', 'special_instruction': 'Your order has been hold!'})

    @api.depends('state')
    def _compute_special_instruction(self):
        for order in self:
            if order.state != 'pending':
                order.special_instruction = 'Your order has been updated.'
            else:
                order.special_instruction = 'Your order is on hold.'
