
# Sale Order Customization Documentation

Module Name: geek_seat_answer
Author: Danial Habibi
Module Version: 0.1
Website: LinkedIn

## How to run This Odoo project
1. Install Docker 
2. run `docker volume db-data-14`
3. run `docker volume web-data-14`
4. run `docker-compose up`
5. fill the form with this
   - master password = `geekseat`
   - database name = `geekseat`
   - email = `admin`
   - password = `admin`
   - check the demo data box
6. go to  http://localhost:8069 in your browser
  

## Overview

The geek_seat_answer module is developed to customize the Sale Order functionality in Odoo. It introduces enhancements and modifications to improve user experience, introduce new fields, and apply custom styles to the user interface.
If You only want to run the addons/module on your own odoo instance, just Copy `geek_seat_answer` folder to your addons folder
  

### Features

##### Custom Special Instruction Field

 - A new field named "Special Instruction" has been added to the Sale
   Order model.
 - This field is used to provide additional instructions or information
   related to an order.

##### New Sale Order State

 - A new state named "Pending" has been introduced in the Sale Order
   workflow. 
 - Orders in the "Pending" state are put on hold.

##### Button Customization
- The "Confirm" button visibility has been customized to only appear when the order is in the "Pending" state.
- The "Cancel" button visibility has been adjusted to be hidden in certain states.

##### UI Improvements

 - CSS styles have been applied to enhance the appearance and layout of
   the Sale Order form view. 
  - Elements have been aligned and styled to
   create a more user-friendly interface. Implementation Details

#### Model Customization
The Sale Order model has been extended with the following changes:
- Addition of the "Special Instruction" field of type Text.
- Introduction of the new "Pending" state in the state selection field.

```
class SaleOrderCustomization(models.Model):
_inherit = 'sale.order'
special_instruction = fields.Text(string="Special Instruction", compute="_compute_special_instruction", store=True)
state = fields.Selection([
('draft', 'Quotation'),
('sent', 'Quotation Sent'),
('pending', 'Pending'),('sale', 'Sales Order'),('cancel', 'Cancelled'),], string='Status', readonly=True, copy=False, index=True, tracking=3,
default='draft', required=True)
```
  

#### Methods to set orders as pending and update special instructions
```
    def action_set_pending(self):
    self.write({'state': 'pending', 'special_instruction': 'Your order has been put on hold.'})   
    @api.depends('state')
    def _compute_special_instruction(self):
	    for order in self:
		    if order.state != 'pending':
		        order.special_instruction = 'Your order has been updated.'
		    else:
			    order.special_instruction = 'Your order is on hold.'
```
##### View Customization

The Sale Order form view has been customized using XML to incorporate the changes and apply new styles 

 - Introduction of the "Special Instruction" field after the "Partner"
   field.
  - Customization of the "Confirm" button to show only in the "Pending"
   state.
   - Customization of the "Cancel" button to be hidden in certain states.
```
    <odoo>
        <data>
        <record  id="view_order_form_inherit"  model="ir.ui.view">
        <!-- ... Existing attributes ... -->
        <field  name="arch"  type="xml">
        <form>
        <field  name="state"  position="replace">
        <!-- ... State field customization ... -->
        </field>
        <xpath  expr="//button[@id='action_confirm']"  position="after">
        <!-- ... Confirm button customization ... -->
        </xpath>
        <xpath  expr="//button[@name='action_cancel']"  position="replace">
        <!-- ... Cancel button customization ... -->
       </xpath>
        <xpath  expr="//field[@name='partner_id']"  position="after">
      <!-- ... Special Instruction field customization ... -->
        </xpath>
		  </form>
	    </field>
	    </record>
	       </data>
    
    </odoo>
```
#### Asset Customization

CSS styles have been applied to enhance the appearance and layout of the Sale Order form view:

A custom CSS file named custom_styles.css has been added in the static/src/css/ directory.

The file path is `/geek_seat_answer/static/src/css/custom_styles.css.`

xml



    <odoo>
    
    <data>
    
    <template  id="assets_backend"  name="Your Custom Module Assets"  inherit_id="web.assets_backend">
    
    <xpath  expr="."  position="inside">
    
    <link  rel="stylesheet"  href="/geek_seat_answer/static/src/css/custom_styles.css"/>
    
    </xpath>
    
    </template>
    
    </data>
    
    </odoo>

#### Testing

 1. Ensure that the module is installed and activated in an Odoo
 2.  instance. Navigate to the Sale Order form view and verify the following: 
     - The "Special Instruction" field appears after the "Partner" field. 
     - The "Confirm" button is visible only in the "Pending" state. 
     - The "Cancel" button visibility is adjusted according to the state. 
     - Custom CSS styles are applied to improve the
    interface layout and appearance.

## Conclusion
The geek_seat_answer module enhances the Sale Order functionality in Odoo by introducing a new field, customizing the state workflow, adjusting button visibility, and improving the user interface with custom CSS styles.
